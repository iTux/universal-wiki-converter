Welcome to the Universal Wiki Converter readme.txt file

This fork aims to add an exporter for Trac and to improve the converter for it.

Building (please note you do not need to build the UWC to run it, just if you're doing development work on it):
To build the UWC use ANT (http://ant.apache.org/):
* cd devel (the devel dir.)
* ant      (the default target will build the UWC under 

To run the newly built UWC
* cd target/uwc/
* chmod a+x *sh
* ./run_uwc_devel.sh

Documentation here: https://migrations.atlassian.net/wiki/display/UWC/Universal+Wiki+Converter

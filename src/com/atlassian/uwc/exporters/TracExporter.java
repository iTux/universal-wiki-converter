package com.atlassian.uwc.exporters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class TracExporter extends SQLExporter {

	private final Logger log = Logger.getLogger(this.getClass());

	private static final String DEFAULT_ENCODING = "utf-8";

	private TracExporterProperties properties;

	@Override
	public void export(@SuppressWarnings("rawtypes") Map propertiesMap)
			throws ClassNotFoundException, SQLException {
		log.debug("export(propertiesMap=" + propertiesMap + ")");

		properties = new TracExporterProperties(propertiesMap);

		connect();

		String outputDirectory = properties.getOutput() + "/trac/";
		TracAttachment attachment = new TracAttachment(outputDirectory);

		String pagesDirectory = outputDirectory + "pages/";

		TracWiki wiki = new TracWiki(pagesDirectory);
		wiki.export();
		attachment.export(TracWikiItem.TYPE);

		if (properties.getConvertBlog()) {
			TracBlog blog = new TracBlog(pagesDirectory);
			blog.export();

			attachment.export(TracBlogItem.TYPE);
		}
	}

	private void writeFile(String path, URL url) {
		try {
			ReadableByteChannel rbc = Channels.newChannel(url.openStream());
			FileOutputStream fos = new FileOutputStream(path);
			fos.getChannel().transferFrom(rbc, 0, 1 << 24);
			fos.close();
		} catch (IOException e) {
			log.error("Problem writing to file: " + path);
			e.printStackTrace();
		}
	}

	private void connect() throws SQLException, ClassNotFoundException {
		// set general authentication.
		Authenticator.setDefault(new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getTracLogin(),
						properties.getTracPassword().toCharArray());
			}

		});

		connectToDB(properties.getJdbcDriver(), properties.getDbUrl(),
				properties.getDbName(), properties.getLogin(),
				properties.getPassword());
	}

	private String getTableName(String tableName) {
		String schema = properties.getDbSchema();
		if (schema != null && !schema.isEmpty()) {
			return schema + "." + tableName;
		}
		return tableName;
	}

	private class TracExporterProperties {
		private static final String PROPERTIES_DB_SCHEMA = "databaseSchema";
		private static final String PROPERTIES_CONVERT_BLOG = "convertBlog";
		private static final String PROPERTIES_PASSWORD = "password";
		private static final String PROPERTIES_LOGIN = "login";
		private static final String PROPERTIES_DRIVER = "jdbc.driver.class";
		private static final String PROPERTIES_DBURL = "dbUrl";
		private static final String PROPERTIES_DBNAME = "databaseName";
		private static final String PROPERTIES_TRAC_LOGIN = "tracLogin";
		private static final String PROPERTIES_TRAC_PASSWORD = "tracPassword";
		private static final String PROPERTIES_TRAC_URL = "tracUrl";
		private static final String PROPERTIES_OUTPUT = "output";
		private static final String PROPERTIES_HISTORY = "history";
		private static final String PROPERTIES_HISTORY_SUFFIX = "history-suffix";

		private final Map<?, ?> properties;

		public TracExporterProperties(Map<?, ?> properties) {
			this.properties = properties;
		}

		public String getOutput() {
			return getString(PROPERTIES_OUTPUT);
		}

		public String getTracLogin() {
			return getString(PROPERTIES_TRAC_LOGIN);
		}

		public String getTracPassword() {
			return getString(PROPERTIES_TRAC_PASSWORD);
		}

		public String getDbName() {
			return getString(PROPERTIES_DBNAME);
		}

		public String getPassword() {
			return getString(PROPERTIES_PASSWORD);
		}

		public String getLogin() {
			return getString(PROPERTIES_LOGIN);
		}

		public String getDbUrl() {
			return getString(PROPERTIES_DBURL);
		}

		public String getJdbcDriver() {
			return getString(PROPERTIES_DRIVER);
		}

		public String getDbSchema() {
			return getString(PROPERTIES_DB_SCHEMA);
		}

		public boolean getConvertBlog() {
			return getBoolean(PROPERTIES_CONVERT_BLOG);
		}

		public String getTracUrl() {
			return getString(PROPERTIES_TRAC_URL);
		}

		public String getHistorySuffix() {
			return getString(PROPERTIES_HISTORY_SUFFIX);
		}

		public boolean isHistoryEnabled() {
			return getBoolean(PROPERTIES_HISTORY);
		}

		private String getString(String key) {
			return (String) properties.get(key);
		}

		private boolean getBoolean(String key) {
			return Boolean.parseBoolean((String) properties.get(key));
		}
	}

	public class TracWiki {

		private final String outputDirectory;

		public TracWiki(String outputDirectory) {
			this.outputDirectory = outputDirectory;
		}

		public void export() throws SQLException {

			String cmd = "select " + TracWikiItem.COLUMN_NAME + ", "
					+ TracWikiItem.COLUMN_VERSION + ", "
					+ TracWikiItem.COLUMN_TIME + ", "
					+ TracWikiItem.COLUMN_AUTHOR + ", "
					+ TracWikiItem.COLUMN_IPNR + ", "
					+ TracWikiItem.COLUMN_TEXT + ", "
					+ TracWikiItem.COLUMN_COMMENT + ", "
					+ TracWikiItem.COLUMN_READONLY + " from "
					+ getTableName("wiki");
			ResultSet entries = sql(cmd);

			try {

				while (entries.next()) {
					TracWikiItem item = new TracWikiItem();
					item.fill(entries);
					item.save(outputDirectory);
				}
			} finally {
				entries.close();
			}
		}
	}

	public class TracBlog {

		private final String outputDirectory;

		public TracBlog(String outputDirectory) {
			this.outputDirectory = outputDirectory;
		}

		public void export() throws SQLException {

			String cmd = "select " + TracBlogItem.COLUMN_NAME + ", "
					+ TracBlogItem.COLUMN_VERSION + ", "
					+ TracBlogItem.COLUMN_TITLE + ", "
					+ TracBlogItem.COLUMN_BODY + ", "
					+ TracBlogItem.COLUMN_PUBLISH_TIME + ", "
					+ TracBlogItem.COLUMN_VERSION_TIME + ", "
					+ TracBlogItem.COLUMN_VERSION_COMMENT + ", "
					+ TracBlogItem.COLUMN_VERSION_AUTHOR + ", "
					+ TracBlogItem.COLUMN_AUTHOR + ", "
					+ TracBlogItem.COLUMN_CATEGORIES + " from "
					+ getTableName("fullblog_posts") + " p where "
					+ TracBlogItem.COLUMN_VERSION + " = (select max("
					+ TracBlogItem.COLUMN_VERSION + ") from "
					+ getTableName("fullblog_posts") + " i where i."
					+ TracBlogItem.COLUMN_NAME + " = p."
					+ TracBlogItem.COLUMN_NAME + ")" + " order by "
					+ TracBlogItem.COLUMN_NAME + ", "
					+ TracBlogItem.COLUMN_VERSION;
			ResultSet entries = sql(cmd);

			try {

				while (entries.next()) {
					TracBlogItem item = new TracBlogItem();
					item.fill(entries);
					item.save(outputDirectory);
					
					exportComments(item);
				}
			} finally {
				entries.close();
			}
		}

		public void exportComments(TracBlogItem item) throws SQLException {

			String cmd = "select " + TracBlogComment.COLUMN_NAME + ", "
					+ TracBlogComment.COLUMN_NUMBER + ", "
					+ TracBlogComment.COLUMN_COMMENT + ", "
					+ TracBlogComment.COLUMN_AUTHOR + ", "
					+ TracBlogComment.COLUMN_TIME + " from "
					+ getTableName("fullblog_comments") + " where name='"
					+ item.getName() + "'";

			ResultSet comments = sql(cmd);

			try {
				while (comments.next()) {
					TracBlogComment comment = new TracBlogComment();
					comment.fill(comments);
					comment.save(outputDirectory);
				}
			} finally {
				comments.close();
			}
		}
	}

	public class TracAttachment {

		private final String outputDirectory;

		public TracAttachment(String outputDirectory) {
			this.outputDirectory = outputDirectory;
		}

		public void export(String type) throws SQLException {
			String cmd = "select " + TracAttachmentItem.COLUMN_ID + ", "
					+ TracAttachmentItem.COLUMN_FILENAME + " from "
					+ getTableName("attachment") + " where type='" + type + "'";

			ResultSet attachments = sql(cmd);

			try {
				while (attachments.next()) {
					TracAttachmentItem attachment = new TracAttachmentItem(type);
					attachment.fill(attachments);
					attachment.save(outputDirectory);
				}
			} finally {
				attachments.close();
			}
		}
	}

	public abstract class TracItem {
		private String name;
		private Integer version;

		public Integer getVersion() {
			return version;
		}

		public void setVersion(Integer version) {
			this.version = version;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String buildFilename(String base, String extension) {

			Pattern colons = Pattern.compile(":|(?:%3A)");
			Matcher colonFinder = colons.matcher(base);
			if (colonFinder.find()) {
				base = colonFinder.replaceAll("__");
			}
			Pattern fileDelims = Pattern.compile("[/\\\\]");
			Matcher delimFinder = fileDelims.matcher(base);
			if (delimFinder.find()) {
				base = delimFinder.replaceAll("_");
			}

			if (this.version != null) {
				if (version != null) {
					String suffix = properties.getHistorySuffix();
					Pattern hash = Pattern.compile("#");
					Matcher hashFinder = hash.matcher(suffix);
					if (hashFinder.find()) {
						extension = hashFinder.replaceFirst(String.format("%04d", version));
					} else
						log.warn("Couldn't find # in history-suffix. Won't be able to preserve histories.");
				}
			}

			return base + extension;
		}

		public abstract String getType();

		public abstract String getContent();

		public void save(String parentDir) {
			File parent = new File(parentDir + getType());
			String filename = this.buildFilename(getFileName(), ".txt");
			String fullpath = parent + File.separator + filename;
			File file = new File(fullpath);
			String message = "";
			try {
				if (!file.getParentFile().exists()) {
					message = "Creating parent directory.";
					file.getParentFile().mkdirs();
					log.debug(message);
				}

				message = "Creating new file: " + fullpath;
				file.createNewFile();
				log.debug(message);
				message = "Sending text to new file: " + fullpath;

				writeToDisk(fullpath);
				log.debug(message);
			} catch (IOException e) {
				log.error("Problem while " + message);
				e.printStackTrace();
			}
		}

		protected String getFileName() {
			return getName();
		}

		protected void writeToDisk(String fullpath) {
			String text = this.getContent();
			writeFile(fullpath, text, DEFAULT_ENCODING);
		}
	}

	public class TracBlogItem extends TracItem {

		public static final String TYPE = "blog";

		public static final String COLUMN_NAME = "name";
		public static final String COLUMN_VERSION = "version";
		public static final String COLUMN_TITLE = "title";
		public static final String COLUMN_BODY = "body";
		public static final String COLUMN_PUBLISH_TIME = "publish_time";
		public static final String COLUMN_VERSION_TIME = "version_time";
		public static final String COLUMN_VERSION_COMMENT = "version_comment";
		public static final String COLUMN_VERSION_AUTHOR = "version_author";
		public static final String COLUMN_AUTHOR = "author";
		public static final String COLUMN_CATEGORIES = "categories";

		private String title;
		private String body;
		private Date publishTime;
		private Date versionTime;
		private String versionComment;
		private String versionAuthor;
		private String author;
		private String categories;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}

		public Date getPublishTime() {
			return publishTime;
		}

		public void setPublishTime(Date publishTime) {
			this.publishTime = publishTime;
		}

		public Date getVersionTime() {
			return versionTime;
		}

		public void setVersionTime(Date versionTime) {
			this.versionTime = versionTime;
		}

		public String getVersionComment() {
			return versionComment;
		}

		public void setVersionComment(String versionComment) {
			this.versionComment = versionComment;
		}

		public String getVersionAuthor() {
			return versionAuthor;
		}

		public void setVersionAuthor(String versionAuthor) {
			this.versionAuthor = versionAuthor;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getCategories() {
			return categories;
		}

		public void setCategories(String categories) {
			this.categories = categories;
		}

		public void fill(ResultSet rs) throws SQLException {
			setName(rs.getString(COLUMN_NAME));
			setVersion(null);
			// setVersion(rs.getInt(COLUMN_VERSION));
			this.title = rs.getString(COLUMN_TITLE);
			this.body = rs.getString(COLUMN_BODY);
			this.publishTime = new Date(rs.getLong(COLUMN_PUBLISH_TIME) * 1000); // blog
																					// uses
																					// seconds
																					// since
																					// epoch
			this.versionTime = new Date(rs.getLong(COLUMN_VERSION_TIME) * 1000); // blog
																					// uses
																					// seconds
																					// since
																					// epoch
			this.versionComment = rs.getString(COLUMN_VERSION_COMMENT);
			this.versionAuthor = rs.getString(COLUMN_VERSION_AUTHOR);
			this.author = rs.getString(COLUMN_AUTHOR);
			this.categories = rs.getString(COLUMN_CATEGORIES);
		}

		@Override
		public String getType() {
			return TYPE;
		}

		@Override
		public String getContent() {
			StringBuffer sb = new StringBuffer();

			sb.append("{trac-export-meta:" + "id=" + this.getName()
					+ "|version=" + this.getVersion() + "|type=" + getType()
					+ "}\n");

			// TODO: FIX TAG HANDLING - if any: sb.append(((tags != null) ?
			// "{tags: " + tags + "}" : ""));
			sb.append("{{title: " + this.title + " }}\n");

			sb.append("{user:" + this.author + "}\n");
			sb.append("{timestamp:" + this.versionTime.getTime() + "}\n");

			sb.append(this.body);

			return sb.toString();
		}
	}

	public class TracBlogComment extends TracItem {

		public static final String TYPE = "blog/comment";

		public static final String COLUMN_NAME = "name";
		public static final String COLUMN_NUMBER = "number";
		public static final String COLUMN_COMMENT = "comment";
		public static final String COLUMN_TIME = "time";
		public static final String COLUMN_AUTHOR = "author";

		private String comment;
		private Date time;
		private String author;
		private int number;

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public Date getTime() {
			return time;
		}

		public void setTime(Date time) {
			this.time = time;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public void fill(ResultSet rs) throws SQLException {
			setName(rs.getString(COLUMN_NAME));
			setVersion(1);
			this.setNumber(rs.getInt(COLUMN_NUMBER));
			this.comment = rs.getString(COLUMN_COMMENT);
			this.time = new Date(rs.getLong(COLUMN_TIME) * 1000); // blog
																	// uses
																	// seconds
																	// since
																	// epoch
			this.author = rs.getString(COLUMN_AUTHOR);
		}

		@Override
		public String getType() {
			return TYPE;
		}

		@Override
		public String getContent() {
			StringBuffer sb = new StringBuffer();

			sb.append("{trac-export-meta:" + "id=" + this.getName()
					+ "|version=" + this.getVersion() + "|type=" + getType()
					+ "}\n");

			sb.append("{user:" + this.author + "}\n");
			sb.append("{timestamp:" + this.time.getTime() + "}\n");

			sb.append(this.comment);

			return sb.toString();
		}

		@Override
		protected String getFileName() {
			return super.getName() + "_" + getNumber();
		}
	}

	public class TracWikiItem extends TracItem {

		public static final String TYPE = "wiki";

		public static final String COLUMN_NAME = "name";
		public static final String COLUMN_VERSION = "version";
		public static final String COLUMN_TIME = "time";
		public static final String COLUMN_AUTHOR = "author";
		public static final String COLUMN_IPNR = "ipnr";
		public static final String COLUMN_TEXT = "text";
		public static final String COLUMN_COMMENT = "comment";
		public static final String COLUMN_READONLY = "readonly";

		private String text;
		private Date time;
		private String author;
		private String ipnr;
		private String comment;
		private boolean readonly;

		public void fill(ResultSet rs) throws SQLException {
			this.setName(rs.getString(COLUMN_NAME));
			this.setVersion(rs.getInt(COLUMN_VERSION));
			this.time = new Date(rs.getLong(COLUMN_TIME) / 1000); // wiki uses
																	// nanoseconds
																	// since
																	// epoch
			this.author = rs.getString(COLUMN_AUTHOR);
			this.ipnr = rs.getString(COLUMN_IPNR);
			this.text = rs.getString(COLUMN_TEXT);
			this.comment = rs.getString(COLUMN_COMMENT);
			this.readonly = rs.getBoolean(COLUMN_READONLY);
		}

		@Override
		public String getType() {
			return TYPE;
		}

		@Override
		public String getContent() {

			StringBuilder sb = new StringBuilder();

			sb.append("{trac-export-meta:" + "id=" + this.getName()
					+ "|version=" + this.getVersion() + "|type=" + getType()
					+ "}\n");

			// TODO: FIX TAG HANDLING - if any: sb.append(((tags != null) ?
			// "{tags: " + tags + "}" : ""));
			sb.append("{{title: " + this.getName() + " }}\n");

			sb.append("{user:" + this.author + "}\n");
			sb.append("{timestamp:" + this.time.getTime() + "}\n");

			sb.append(this.text);

			return sb.toString();
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public Date getTime() {
			return time;
		}

		public void setTime(Date time) {
			this.time = time;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getIpnr() {
			return ipnr;
		}

		public void setIpnr(String ipnr) {
			this.ipnr = ipnr;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public boolean isReadonly() {
			return readonly;
		}

		public void setReadonly(boolean readonly) {
			this.readonly = readonly;
		}
	}

	public class TracAttachmentItem extends TracItem {

		public static final String COLUMN_ID = "id";
		public static final String COLUMN_FILENAME = "filename";

		private String id;
		private final String type;

		public TracAttachmentItem(String type) {
			this.type = type;
		}

		public URL getUrl() {
			URL url = null;
			try {
				URL u = new URL(properties.getTracUrl() + "raw-attachment/"
						+ type + "/" + this.id + "/" + this.getName());
				url = new URI(u.getProtocol(), u.getUserInfo(), u.getHost(),
						u.getPort(), u.getPath(), u.getQuery(), u.getRef())
						.toURL();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			return url;
		}

		@Override
		public String getType() {
			return "attachment";
		}

		public void fill(ResultSet rs) throws SQLException {
			this.setName(rs.getString(COLUMN_FILENAME));
			this.id = rs.getString(COLUMN_ID);
		}

		@Override
		public String buildFilename(String base, String extension) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(this.type);
			stringBuilder.append(File.separator);
			stringBuilder.append(super.buildFilename(this.id, ""));
			stringBuilder.append(File.separator);
			stringBuilder.append(getName());
			return stringBuilder.toString();
		}

		@Override
		public String getContent() {
			// no content, as all attachments are retrieved form the running
			// Trac instance.
			return null;
		}

		@Override
		protected void writeToDisk(String fullpath) {
			writeFile(fullpath, this.getUrl());
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

	}

}
